
Data Types
==========

Right now there is a limited set of data types and text is always encoded as UTF-8.  Feel
free to open an `issue <https://github.com/mkleehammer/pglib/issues>`_ to request new ones.

.. _paramtypes:

Parameter Types
---------------

Parameters of the following types are accepted:

+--------------------------+------------------+
| Python Type              | SQL Type         |
+==========================+==================+
| None                     | NULL             |
+--------------------------+------------------+
| bool                     | boolean          |
+--------------------------+------------------+
| bytes                    | bytea            |
+--------------------------+------------------+
| bytearray                | bytea            |
+--------------------------+------------------+
| datetime.date            | date             |
+--------------------------+------------------+
| datetime.datetime        | timestamp        |
+--------------------------+------------------+
| datetime.time            | time             |
+--------------------------+------------------+
| datetime.timedelta       | interval         |
+--------------------------+------------------+
| decimal.Decimal          | numeric          |
+--------------------------+------------------+
| float                    | float8           |
+--------------------------+------------------+
| int                      | int64 or numeric |
+--------------------------+------------------+
| str                      | text (UTF-8)     |
+--------------------------+------------------+
| uuid.UUID                | uuid             |
+--------------------------+------------------+
| tuple<int>, list<int>    | int[]            |
+--------------------------+------------------+
| tuple<str>, list<str>    | str[]            |
+--------------------------+------------------+
| tuple<date>, list<date>  | date[]           |
+--------------------------+------------------+
| pglib.hstore(dict)       | hstore           |
+--------------------------+------------------+
| dict                     | json             |
+--------------------------+------------------+
| pglib.json(obj)          | json             |
+--------------------------+------------------+

Arrays can only contain one type, so tuples and lists must contain elements of all of the same
type.  Only strings and integers are supported at this time.  Note that a list or tuple can
contain None, but it must contain at least one string or integer so the type of array can be
determined.

Dictionaries are automatically sent as JSON.  If you want to send other Python objects has
JSON, you must wrap them in a pglib.json object::

  param = pglib.json([1, 2, 3])
  cnxn.execute("insert into t(j) values ($1)", param)

Similarly, if you want to pass a dictionary to an hstore column, wrap it in a ``pglib.hstore`` object::

  param = pglib.hstore({"one": "two"})
  cnxn.execute("insert into t(h) values ($1)", param)

.. _resulttypes:

Result Types
------------

The following data types are recognized in results:

+---------------------------+--------------------+
| SQL Type                  | Python Type        |
+===========================+====================+
| NULL                      | None               |
+---------------------------+--------------------+
| boolean                   | bool               |
+---------------------------+--------------------+
| bytea                     | bytes              |
+---------------------------+--------------------+
| char, varchar, text       | str (UTF-8)        |
+---------------------------+--------------------+
| float4, float8            | float              |
+---------------------------+--------------------+
| smallint, integer, bigint | int                |
+---------------------------+--------------------+
| money                     | decimal.Decimal    |
+---------------------------+--------------------+
| numeric                   | decimal.Decimal    |
+---------------------------+--------------------+
| date                      | datetime.date      |
+---------------------------+--------------------+
| time                      | datetime.time      |
+---------------------------+--------------------+
| timestamp / timestamptz   | datetime.datetime  |
+---------------------------+--------------------+
| uuid                      | uuid.UUID          |
+---------------------------+--------------------+
| int[]                     | list<int>          |
+---------------------------+--------------------+
| text[]                    | list<str>          |
+---------------------------+--------------------+
| date[]                    | list<date>         |
+---------------------------+--------------------+
| hstore                    | dict               |
+---------------------------+--------------------+
| json & jsonb              | dict               |
+---------------------------+--------------------+

Python's ``timedelta`` only stores days, seconds, and microseconds internally, so intervals
with year and month are not supported.
