
void RegisterLTree(Oid oid);
bool IsLTree(Oid oid);
Oid GetLTreeOid();
bool IsLTreeRegistered();
PyObject* GetLTree(const char* p);
